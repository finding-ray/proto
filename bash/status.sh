#!/bin/bash
#
# Script for monitoring the status of gitlab-runners
# in order to troubleshoot failures during the intensive
# GNU Radio build process.
#
# Displays disk, CPU, RAM, and Swap usage as well as
# number of processes and other useful statistics.
#
# Run with watch to monitor the system:
#
#   watch bash status.sh 

df -h

echo ''
echo ''

swapon --summary

echo ''
echo ''

top -b -n 1
