=====
proto
=====

Prototyping for Finding Mr Ray, early project documentation and scripts.


Dependancies
============

-   `GNU Radio <http://gnuradio.org/>`_
-   `gr-gsm <https://github.com/ptrkrysik/gr-gsm>`_


Install
=======

See the docs at `http://finding-ray.gitlab.io/proto/ <http://finding-ray.gitlab.io/proto/>`_.
