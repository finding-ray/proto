Install Hypriot
===============

Download the latest `Hypriot image <https://blog.hypriot.com/downloads/>`_ then
extract the zip file::

    cd ~/Downloads
    unzip hypriot-rpi-v?.?.?.img.zip

Check to see which device the SD card is, this can be done either by running
`dmesg -T | grep mount` and checking the output for the device that
corresponds with the time the SD card was mounted, or using `lsblk -a` to
identify by size or path. If there is anything in the `MOUNTPOINT` column
of `lsblk -a` output for the SD card it must be unmounted using the
`unmount` command::

    umount /media/username/some_device_id

Use the `Hypriot flash tool <https://github.com/hypriot/flash/>`_ 
to flash the image to the SD card. This will look something like::

    curl -O https://raw.githubusercontent.com/hypriot/flash/master/$(uname -s)/flash
    chmod +x flash
    sudo mv flash /usr/local/bin/flash # only if you want global install
    sudo apt-get install -y pv curl python-pip unzip hdparm
    sudo pip install awscli
    ./flash -c device-init.yaml -C config.txt -d /dev/sd? \
        https://link-to-image.com/hypriot-rpi.img.zip

Suggested ``config.txt``::

    bootcode_delay=2
    boot_delay=2
    disable_splash=1
    lcd_rotate=2

``device-init.yaml`` format::

    hostname: awesome
    wifi:
        interfaces:
            wlan0:
                ssid: "my-ssid"
                password: "my_super_secret_password"

Boot the Pi with the SD card and run `sudo ping 8.8.8.8` to ensure a
working network connection, then also ping a web address to check
that DNS is also working. The `/boot/device-init.yaml` file will be
used to overwrite reconfigure the network settings each boot sequence
so it must be correct on boot.
