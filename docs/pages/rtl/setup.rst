Using a RTL-SDR
===============

Note, the E4000 tuner is best. Beware any model other than the 2838 with the 
E4000, others will not have the complete range. The 2832 for example will not
cover the 1900MHz GSM band.

Install the driver::

    # apt-get install libusb-1.0

Verify that it is attached and seen::

    $ lsusb | grep -i RTL
    Bus 007 Device 005: ID 0bda:2838 Realtek Semiconductor Corp. RTL2838 DVB-T

Run a benchmark to test it::

    $ rtl_test -t
    Found 1 device(s):
    0:  Realtek, RTL2838UHIDIR, SN: 00000001

    Using device 0: Generic RTL2832U OEM
    Found Rafael Micro R820T tuner
    Supported gain values (29): 0.0 0.9 1.4 2.7 3.7 7.7 8.7 12.5 14.4 15.7 16.6 19.7 20.7 22.9 25.4 28.0 29.7 32.8 33.8 36.4 37.2 38.6 40.2 42.1 43.4 43.9 44.5 48.0 49.6 
    Sampling at 2048000 S/s.
    No E4000 tuner found, aborting.

If you get an about permissions like this::

    Using device 0: Generic RTL2832U OEM
    usb_open error -3
    Please fix the device permissions, e.g. by installing the udev rules file rtl-sdr.rules
    Failed to open rtlsdr device #0.
    
Add a line to ``/etc/udev/rules.d/rtl-sdr.rules`` to
set up correct permissions using the ID value from the ``lsusb`` output.
Using the correct vendor and product ids for the particular device this
will make it accessible to anyone in the ``adm`` group and add a ``/dev/rtl_sdr``
symlink when the device is attached.

::

    SUBSYSTEMS=="usb", ATTRS{idVendor}=="0bda", ATTRS{idProduct}=="2832", 
    GROUP="adm", MODE:="0666", SYMLINK+="rtl_sdr"

Remove the device, restart udev and plug it back in::

    # service udev restart

If you get a message about the device already being in use like
this::

    Found 1 device(s):
    0:  Realtek, RTL2838UHIDIR, SN: 00000001

    Using device 0: Generic RTL2832U OEM

    Kernel driver is active, or device is claimed by second instance of librtlsdr.
    In the first case, please either detach or blacklist the kernel module
    (dvb_usb_rtl28xxu), or enable automatic detaching at compile time.

    usb_claim_interface error -6
    Failed to open rtlsdr device #0.

This means ``dvb_usb_rtl28xxu`` is already using the device and it
must be unloaded::

    # rmmod dvb_usb_rtl28xxu

It can also be permanently blacklisted by putting an entry in
``/etc/modprobe.d`` named a file like ``rtl-sdr.conf`` with the lines::

    blacklist dvb_usb_rtl28xxu
    blacklist rtl2832
    blacklist rtl2830

Calibration
-----------

It is also a good idea to calibrate it, first install Kalibrate,
note that this will not work on ARMv7::

    $ pybombs install kal
    $ kal -s GSM900

This will give us the average absolute error in PPM:

ARMv7 Kalibrate
---------------

    git clone https://github.com/asdil12/kalibrate-rtl.git
    cd kalibrate-rtl
    git checkout arm_memory
    ./bootstrap
    ./configure
    make
    sudo make install
