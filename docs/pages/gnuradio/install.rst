Install & Configure GNU Radio
=============================

`PyBOMBS <http://gnuradio.org/redmine/projects/pybombs/wiki>`_ is a dependency
manager for GNU Radio. The 
`PyBOMBS manual and repository <https://github.com/gnuradio/pybombs/>`_
can be very useful for getting started.

Debian
------

First install ``sudo`` and configure your user account with permissions to use it for
package installation. If attempt install without sudo permissions the install
will fail without any form of useful error message about what went wrong.
PyBOMBS assumes it has root privileges for system package installation.

::

    # apt-get install sudo
    # echo username ALL = PASSWD : /usr/bin/apt-get , /usr/bin/aptitude , /usr/bin/apt , /usr/bin/pip >> /etc/sudoers

Install GNU Radio locally, after the last command to install the default GNU
Radio libraries go get a coffee. The build took about an hour on my workstation
and about 4 hours on my laptop.

::

    # apt-get install python-pip autotools-dev autoconf
    # apt-get install cmake libboost-all-dev libcppunit-dev swig doxygen liblog4cpp5-dev python-scipy
    # aptitude install libffi-def libssl-dev
    # pip install pybombs
    $ pybombs recipes add gr-recipes git+https://github.com/gnuradio/gr-recipes.git
    $ pybombs recipes add gr-etcetera git+https://github.com/gnuradio/gr-etcetera.git
    $ mkdir ~/prefix/
    $ pybombs prefix init -a default ~/prefix/default/ -R gnuradio-default

Install ``gr-gsm``, ``gr-cdma`` and ``gr-lte``  which provides all the GNU Radio features 
for all cellphone radio technologies, it will be downloaded and built requiring
~3.5GB of space.

::

    $ pybombs install gr-gsm
    $ pybombs install gr-cdma
    $ pybombs install gr-lte

Tell gnuradio-companion where to find custom blocks of ``gr-gsm`` in
``~/.gnuradio/config.conf`` with:

::

    [grc]
    local_blocks_path=/home/username/prefix/default/src/gnuradio/grc/blocks/


Install on Raspberry Pi 2 with Raspbian Lite OS
-----------------------------------------------

Install ``pip`` to install PyBOMBS with, you need to start somewhere. Also all of
the tools PyBOMBS will need to use when building and installing everything.

::

    # apt-get install python-pip autotools-dev autoconf cmake libboost-all-dev libcppunit-dev swig doxygen liblog4cpp5 python-scipy libffi-dev libssl-dev
    # pip install pybombs

Set up PyBOMBS with some repositories to pull from and install the default
GNU Radio packages.

::

    $ pybombs recipes add gr-recipes git+https://github.com/gnuradio/gr-recipes.git
    $ pybombs recipes add gr-etcetera git+https://github.com/gnuradio/gr-etcetera.git
    $ mkdir ~/prefix/
    $ pybombs prefix init -a default ~/prefix/default/ -R gnuradio-default

Get a coffee, go about your day, and go to sleep this is going to take a while.

Build fails on ``gnuradio`` build with following output (irrelevant output cut):

::

    ...
    ...
    ...
    Assembler messages:
    Error: selected processor does not support ARM mode `sbfx r11,r1,#2,#1` 
    ...
    ...
    ...

Problem is that the ``sbfx`` assembler instruction is not supported by ARMv6
which Raspian is built for, even though it runs on a ARMv7 chip. The
solution is to replace the ``spfx`` command with a compatible assembly
instruction for ARMv6 and comment out the ``spfx`` instruction with
``@``, and it will build and work fine::

    mov     r11, #0
    tst      r1, #4
    movne   r11, #15

The run time at failure was 6 hours and the remaining disc space is ~2GB
which may not be enough to build ``gr-gsm``, this problem may need to be
solved with cross compilation.


Install on Raspberry Pi 3 with Raspbian OS
------------------------------------------

Install ``pip`` to install PyBOMBS with, you need to start somewhere. Also all of
the tools PyBOMBS will need to use when building and installing everything.

::

    # apt-get install python-pip autotools-dev autoconf cmake libboost-all-dev libcppunit-dev swig doxygen liblog4cpp5 python-scipy libffi-dev libssl-dev
    # pip install pybombs

Set up PyBOMBS with some repositories to pull from and install the default
GNU Radio packages.

::

    $ pybombs recipes add gr-recipes git+https://github.com/gnuradio/gr-recipes.git
    $ pybombs recipes add gr-etcetera git+https://github.com/gnuradio/gr-etcetera.git
    $ mkdir ~/prefix/

The system does not have a lot of RAM to work with, without setting up swap
for the system to use it will likely lock up. There are two ways to deal with
this, first Raspian uses ``dphys-swapfile`` the parameters of which can be changed
in ``/etc/dphys-swapfile`` by default this is set to 100MB, the system could easily
use a few GB for the build. Change this to ``CONF_SWAPSIZE=4000`` if you have the
space, but note that this will decrease the lifespan of a SD card. The other
option is to plug in an external drive such as a USB drive that you are ok
with destroying all the data on. To do this insert the drive, find out what
the device is by running ``dmesg`` and looking at the tail of the output. Then
run ``mkswap /dev/sdx`` and ``swapon /dev/sdx``, note that this will destroy all
data on the device.

Also the version of GCC installed from the Raspbian Repo is compiled for the
Raspberry Pi 1 which is ARMv6. This forces certain older C flags during the
compilation that breaks the GNU Radio build, specifically when compiling Volk.
One possible solution is to replace the offending ARMv6 instruction (``spfx``) 
with an equivalent instruction, or do the following to update GCC.

::

    # apt-get install libssl-dev libtiff5-dev
    # echo deb http://mirrors.acm.jhu.edu/debian/ jessie main contrib non-free >> /etc/apt/sources.list
    # echo deb http://mirrordirector.raspbian.org/raspbian/ jessie rpi >> /etc/apt/sources.list
    # apt-get update
    # apt-get install debian-archive-keyring
    # apt-get update
    # apt-get dist-upgrade
    # apt-get install gcc-4.9 gcc-4.9-base libgcc-4.9-dev libgcc1 gcc

And add the following to ``config_opt`` in ``~/.pybombs/recipes/gr-recipes/gnuradio.lwr``::

    vars:
    config_opt: " -DENABLE_DOXYGEN=$builddocs -DCMAKE_ASM_FLAGS='-march=armv7-a -mthumb-interwork -mfloat-abi=hard -mfpu=neon -mtune=cortex-a7' -DCMAKE_C_FLAGS='-march=armv7-a -mthumb-interwork -mfloat-abi=hard -mfpu=neon -mtune=cortex-a7'"

Lastly remove some old Deb packages that cause problems::

    # apt-get remove libqtgui4 libqtcore4 libqt4-xmlpatterns libqt4-xml libqt4-network libqt4-dbus

And finally install::

    $ pybombs prefix init -a default ~/prefix/default/ -R gnuradio-default

Now get a coffee, go about your day, and go to sleep this is going to take a while.
It took about 24 hours on my Raspberry Pi 3.

Install ``gr-gsm``, ``gr-cdma`` and ``gr-lte``  which provides all the GNU Radio features 
for all cellphone radio technologies, it will be downloaded and built requiring
~3.5GB of space. 

::

    $ pybombs install gr-gsm
    $ pybombs install gr-cdma
    $ pybombs install gr-lte

Tell gnuradio-companion where to find custom blocks of ``gr-gsm`` in
``~/.gnuradio/config.conf`` with::

    [grc]
    local_blocks_path=/home/username/prefix/default/src/gnuradio/grc/blocks/


Install on Other Systems
------------------------
 
First remove and replace defective operating system::

    # rm -rf /
    # apt-get install debian

No but I really have no idea, good luck.
