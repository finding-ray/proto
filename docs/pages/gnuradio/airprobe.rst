GNU Radio Airprobe
==================

The application to set up GNU Radio blocks for GSM is found at
``~/prefix/default/src/gr-gsm/apps/grgsm_livemon.grc``. To use it
a software defined radio must first be configured on the system.
To start GNU Radio companion run::

    $ source ~/prefix/default/setup_env.sh
    $ gnuradio-companion

It is also possible to run specific applications by providing them as input::

    $ gnuradio-companion grgsm_livemon
    $ gnuradio-companion grgsm_scanner
    $ gnuradio-companion airprobe_rtlsdr.py

To run the Airprobe program directly::

    $ source ~/prefix/default/setup_env.sh
    $ airprobe_rtlsdr.py

Tune it into the GSM frequency range and scan untill you get something like
the following streaming across the terminal you ran the program from.

::

     41 06 1c 13 00 62 7e c3 60 04 b9 00 00 64 51 82 81 80 00 5b 2b 2b 2b
     15 06 21 00 01 f0 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     41 06 21 a0 05 f4 cd 08 62 fc 17 05 f4 fd 98 cb 9e 27 2b 2b 2b 2b 2b
     25 06 21 20 05 f4 e0 f1 45 9e 23 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     41 06 21 a0 05 f4 da 99 4f 74 17 05 f4 d7 91 bf d9 27 2b 2b 2b 2b 2b
     25 06 21 20 05 f4 02 7c 0f cb 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     15 06 21 00 01 f0 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     15 06 21 00 01 f0 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     25 06 21 20 05 f4 d5 91 70 d7 23 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     41 06 21 a0 05 f4 17 ab d0 5d 17 05 f4 e3 0c 96 e7 2f 2b 2b 2b 2b 2b
     55 06 19 8f 5a 00 80 00 00 00 00 00 00 00 00 00 00 00 00 b9 00 00 6b
     15 06 21 00 01 f0 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     2f 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     25 06 21 20 05 f4 e1 93 68 f3 23 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     15 06 21 00 01 f0 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     25 06 21 20 05 f4 f8 8a 3e 53 23 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     15 06 21 00 01 f0 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     25 06 21 20 05 f4 e0 99 e4 d4 23 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     15 06 21 00 01 f0 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     25 06 21 20 05 f4 d0 88 a1 a4 23 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b 2b
     41 06 21 a0 05 f4 c8 f2 10 ff 17 05 f4 37 9f dd f9 23 2b 2b 2b 2b 2b

This is the GSM downlink data in raw hexadecimal representation of the bits.
What the ``airprobe_rtlsdr.py`` program does with this data is send it to a
UDP socket. From the loopback interface we can pipe it into a FIFO for analysis. For now this
can be done using wireshark, so lets install that. There are significant 
security risks with running wireshark as root so consider setting it up
to be used as a user instead of root.

::

    # apt-get install wireshark

To run wireshark and point it to listen for GSM data on the loopback
interface run::

    $ wireshark -k -Y '!icmp && gsmtap' -i lo

Now wireshark will start pulling in the GSM data and reading it.
System information will be visible along with identity codes,
pager requests, type of pager request, signal to noise ratio,
requests, etc.
This data can be collected in `.pcap` or packet capture files which
can then be analysed for detecting suspicious cell tower activities.
