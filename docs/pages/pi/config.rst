Raspberry Pi Setup
==================

Install `Raspbian <https://www.raspberrypi.org/downloads/raspbian/>`_ the unofficial
Debian derivative for the raspberry pi. Note that this will contain proprietary
software including the Java SE Platform from Oracle and Mathematica from
Wolfram Research Inc. a free alternative is the Banana Pi. Alternatively to 
make better use of a smaller SD card, or save space for the
~4GB that are needed to build the software on the pi, Raspbian Lite can be
installed. 

Immediately after install and booting you are greeted with user authentication,
the default username is ``pi`` and the password is ``raspberry``. A new user can
be created with ``adduser`` and passwords can be changed with the ``passwd``
command. Note that in order to delete the user ``pi`` autologin must be disabled
for the account in ``/etc/lightdm/lightdm.conf`` and stop the graphical display
manager with ``service lightdm stop``. Then you must disable autologin on TTY1
by changing a symlink the security incompetent creators of Raspian manipulated::

    ln -fs /lib/systemd/system/getty@.service \
        /etc/systemd/system/getty.target.wants/getty@tty1.service

Always keep the system up to date with::


    # apt-get update && apt-get upgrade

Installing a GUI
----------------

If installing Raspbian Lite the Graphical User Interface must be
installed manually after setup. Installing the lite version will save around
1.5GB after installing the desktop environment on it. To install the desktop
environment run::

    # apt-get install --no-install-recommends xserver-xorg
    # apt-get install --no-install-recommends xinit

Then update the system::

    # apt-get update && apt-get upgrade

Then decide to run either LXDE, XFCE, or MATE for the desktop environment::

    # apt-get install lxde-core lxappearance

Or::

    # apt-get install xfce4 xfce4-terminal

Or::

    # apt-get install mate-desktop-environment-core

To be greeted with a login screen at boot a Graphical Display Manager (GDM)
is needed. LightDM works well in resource restricted environments.

::

    # apt-get install lightdm

GNU Radio needs the system PYTHONPATH to include executable programs it may use
such as ``airprobe_rtlsdr.py``. If installed locally this path is located 
somewhere in the ``~/prefix`` directory. If using bash on GNU/Linux run the
following command to add the needed directory.

::

    $ echo export PYTHONPATH="\${PYTHONPATH}:~/prefix/bin" >> ~/.bashrc
    $ export PYTHONPATH=${PYTHONPATH}:~/prefix/bin
    $ airprobe_rtlsdr.py


Freeing Raspian
---------------

Raspian comes with some proprietary software that is not only unneeded and
taking up a lot of space but also a security risk. To remove the software run::

    # apt-get remove wolfram-engine scratch minecraft-pi sonic-pi dillo penguinspuzzle oracle-java8-jdk oracle-java7-jdk
    # apt-get remove libreoffice*
    # apt-get remove 

This will free over a gigabyte of space.


Touch Keyboard
--------------


If using a touch screen interface install the on screen keyboard::

    # apt-get install matchbox-keyboard

Or::

    # apt-get install at-spi2-core florence

Screen Saver
------------

If it is a development Pi then the screensaver should probably be disabled and
possibly also the energy saving features. Add the following in 
``/etc/X11/xinit/xinitrc``.

::

    xset s off         # don't activate screensaver
    xset -dpms         # disable DPMS (Energy Star) features.
    xset s noblank     # don't blank the video device

Run from terminal for them to take effect immediately.  
