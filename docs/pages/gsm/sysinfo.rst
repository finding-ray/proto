System Information Messages
===========================

Metrics will be based on the system information messages from
which we will need to extract the needed information.
System messages are sent unencrypted and contain all the
information needed to communicate with the network.

*   :abbr:`LAC (Location Area Code)`: Unique number assigned to an area, used by a group of :abbr:`BTSs (Base Transceiver Stations)`.
*   :abbr:`CID (Cell ID)`: Unique number assigned to each :abbr:`BTS (Base Transceiver Station)`.
*   :abbr:`ARFCN (Absolute Radio Frequency Channel Number)`.
*   :abbr:`MCC (Mobile Country Code)`: Identifies the country.
*   :abbr:`MNC (Mobile Network Code)`: Identifies the service provider.
*   :abbr:`IMEISV (International Movile Equipment Identity / Software Version)`: Unique number to the physical phone, used to blacklist stolen phones regardless if the :abbr:`SIM (Subscriber Identity Module)` card has been changed or not.


System Information Message
--------------------------

*   *Type 1:* Channel Type :abbr:`BCCH (Broadcast Control Channel)`

    *   List of :abbr:`ARFCNs (Absolute Radio Frequency Channel Numbers)` of the cell
    *   :abbr:`RACH (Random Access Chanel)` control parameters

*   *Type 2:* Channel Type :abbr:`BCCH (Broadcast Control Channel)`

    *   Neighbor cell description (list of :abbr:`ARFCNs (Absolute Radio Frequency Channel Numbers)` of neighboring cell)
    *   List of :abbr:`BCCH (Broadcast Control Channel)` frequencies

*   *Type 3:* Channel Type :abbr:`BCCH (Broadcast Control Channel)`

    *   :abbr:`CID (Cell ID)` decoded
    *   :abbr:`LAI (Location Area Identity)` ( :abbr:`MCC (Mobile Country Code)` + :abbr:`MNC (Mobile Netowrk Code)` + :abbr:`LAC (Location Area Code)` )
    *   :abbr: `GPRS (General Packet Radio Service)` information

*   *Type 4:* Channel Type :abbr:`BCCH (Broadcast Control Channel)`

    *   :abbr:`LAI (Location Area Identity)` ( :abbr:`MCC (Mobile Country Code)` + :abbr:`MNC (Mobile Netowrk Code)` + :abbr:`LAC (Location Area Code)` )
    *   Cell selection parameters
    *   :abbr:`RACH (Random Access Chanel)` control parameters
    *   :abbr: `GPRS (General Packet Radio Service)` information

*   *Type 2ter:* Channel Type :abbr:`BCCH (Broadcast Control Channel)`

    *   This is a neighbor cell description verbose
    *   list of :abbr:`ARFCNs (Absolute Radio Frequency Channel Numbers)` of neighboring cell
    *   List of :abbr:`BCCH (Broadcast Control Channel)` frequencies

*   *Type 2quater:* Channel Type :abbr:`BCCH (Broadcast Control Channel)`

    *   3G neighbor cell description

*   *Type 13:* Channel Type :abbr:`BCCH (Broadcast Control Channel)`

    *   :abbr: `GPRS (General Packet Radio Service)` information (Cell options, power control parameters, etc.)


Paging Request Message
----------------------

*   *Type 1:* Channel Type :abbr:`CCCH (Common Control Channel)`

    *   :abbr:`IMSI (International Mobile Subscriber Idenity)`
    *   Page Mode = Normal paging (0)
    *   Channel Needed
    *   :abbr:`TIMSI (Temporary International Mobile Subscriber Identity)`/:abbr:`P-TMSI (Packet-Temporary Mobile Subscriber Identity)`

*   *Type 2:* Channel Type :abbr:`CCCH (Common Control Channel)`

    *   Page Mode = Normal paging (0)
    *   Channel Needed
    *   Mobile Identity 1, 2 = :abbr:`TIMSI (Temporary International Mobile Subscriber Identity)`/:abbr:`P-TMSI (Packet-Temporary Mobile Subscriber Identity)` or :abbr:`IMSI (International Mobile Subscriber Idenity)` Mobile Identity 3

::

            0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f              
    0000   00 00 00 00 00 00 00 00 00 00 00 00 08 00 45 00
    0010   00 43 1c a6 40 00 40 11 20 02 7f 00 00 01 7f 00
    0020   00 01 c2 e4 12 79 00 2f fe 42 02 04 01 00 00 00
    0030   c9 00 00 16 20 e3 02 00 04 00 55 06 22 00 yy yy
    0040   yy yy zz zz zz 4e 17 08 XX XX XX XX XX XX XX XX
    0050   8b
    yy yy yy yy = TMSI/P-TMSI - Mobile Identity 1
    zz zz zz zz = TMSI/P-TMSI - Mobile Identity 2
    XX XX XX XX XX XX XX XX = IMSI

*   *Type 3:* Channel Type :abbr:`CCCH (Common Control Channel)`

    *   Mobile Identity 1, 2, 3, and 4 = :abbr:`TIMSI (Temporary International Mobile Subscriber Identity)`/:abbr:`P-TMSI (Packet-Temporary Mobile Subscriber Identity)`
    *   Page Mode = Normal paging (0)
    *   Channel Needed

Immediate Assignment Message
----------------------------

*   Channel Type :abbr:`CCCH (Common Control Channel)`
*   Time Advance Value
*   Packet Channel Description (Time Slot)
*   Page Mode = Extended Paging (1)

Citations
---------

.. [Oros42] ` <https://github.com/Oros42/IMSI-catcher>`_

.. [GV2015] `Grigoris Valtas's Master Thesis "Exploiting Chronic Vulnerabilities in GSM by Using Commodity Hardware and Software", University of Piraeus, September 2015 <http://dione.lib.unipi.gr/xmlui/bitstream/handle/unipi/8669/Valtas_Grigoris.pdf?sequence=1>`_

.. [ShareTech] `Share Technote <http://www.sharetechnote.com/>`_
