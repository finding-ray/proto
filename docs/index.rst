=====
proto
=====

This is the documentation of **proto**, it consists of early prototyping
hows and whys for Finding Mr Ray.

Quick Setup
-----------

Install development environment::

    # apt-get install python3 python3-venv python3-dev pip3
    # exit
    $ mkdir ~/.virtualenv
    $ python3 -m venv ~/.virtualenv/proto
    $ source ~/.virtualenv/proto/bin/activate
    $ pip install -r requirements.txt
    $ pip install -r requirements-test.txt
    $ pip install -U setuptools

To build docs run from project root::

    $ python setup.py docs

.. note::

    `virtualenv` is depreciated in Python3 in favor of the new built in
    method above `python3 -m venv env` for creating virtual environments.

Available Prototyping Tools
---------------------------

3D printers

    -   Dragas Computer Lab: 
    -   Strome Entrepreneurial Center


Operating System
----------------

The only touch friendly operating system that could be used is Android,
there is an experimental build of GNU Radio for android but it will need to
be tested to see if it will work. This would be very much ideal if a touch
interface is used. Running android on a Pi is also somewhat experimental 
so this may not be feasible.

`GNU Radio Android Project <http://gnuradio.org/redmine/projects/gnuradio/wiki/Android>`_


Contents
========

.. toctree::
    :maxdepth: 2

    Prototype Hardware <pages/hardware/prototype>
    GNU Radio Instalation <pages/gnuradio/install>
    GNU Radio Airprobe <pages/gnuradio/airprobe>
    RTL-SDR <pages/rtl/setup>
    Hypriot <pages/hypriot/install>
    Raspian <pages/pi/config>
    GSM Network <pages/gsm/sysinfo>
    Continuous Integration <pages/continuous_integration/setup>
    License <license>
    Authors <authors>
    Changelog <changes>
    Module Reference <api/modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
